<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckifAuthedIsProvider
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->hasRole('provider')) {
            return $next($request);
        } else {
            return response()->json(['message' => 'نعتذر هذا التطبيق مخصص لاصحاب المحلات التجارية'], 500);
        }
    }
}
