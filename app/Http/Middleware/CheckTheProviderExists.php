<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class CheckTheProviderExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $user =User::where('mobile_number',$request->mobile_number)->first();

            if(is_null($user))
            {
                throw new \Exception('اسم المستخدم او كلمة المرور غير صحيحة');
            }
            if($user->role !== 'provider')
            {
                throw new \Exception('نعتذر هذا التطبيق مخصص لاصحاب المحلات التجارية');
            }

            return $next($request);

        }catch(\Exception $e){
            return response()->json(['message' => $e->getMessage()],500);
        }

    }
}
