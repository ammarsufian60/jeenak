<?php

namespace App\Notifications;

use App\Domains\User\Actions\StoreProviderNotifications;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Neo\PusherBeams\PusherBeams;
use Neo\PusherBeams\PusherMessage;

class PushAcceptedOrderNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $branch;

    public function __construct($branch)
    {
        $this->branch= $branch;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [PusherBeams::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    public function toPushNotification($notifiable)
    {
        (new StoreProviderNotifications($this->branch->manager->id,'طلب جديد','طلب جديد','NEWORDER'))->handle();

        return PusherMessage::create()
            ->iOS()
            ->sound('success')
            ->title('طلب جديد')
            ->body("طلب جديد");
    }

    public function pushNotificationInterest()
    {
        return "new_order_".$this->branch->id;
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
