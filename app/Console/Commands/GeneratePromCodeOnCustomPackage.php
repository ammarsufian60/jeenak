<?php

namespace App\Console\Commands;

use App\Domains\Shipment\Models\PromoCode;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GeneratePromCodeOnCustomPackage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'promocode:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate promo code ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        for($i=0;$i<9000;$i++){
            PromoCode::create([
                'code'=> mt_rand(100000000,999999999),
                'work_at' => Carbon::now()->format(''),
                'expired_at' => Carbon::now()->format(''),
                'discount' => 10,
                'type' => 'package',
                'counter' => 1,
                'package_id' => 1
            ]);
        }
    }
}
