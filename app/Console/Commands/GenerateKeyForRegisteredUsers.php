<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class GenerateKeyForRegisteredUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'referral:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        collect(User::all())->each(function ($user){
            $length = strlen($user->mobile_number);
           $user->update([
               'referral_key'=> substr($user->mobile_number,$length-4,$length-1).mt_rand(10000,99999),
           ]);
        });
    }
}
