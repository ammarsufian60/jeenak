<?php

namespace App\Policies;

use App\Domains\User\Models\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Check to blanket-add all permissions.
     *
     * @param User|null $user
     * @param $ability
     *
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->hasRole('super-admin')) {
            return true;
        }
    }

    /**
     * @param User $user
     * @param Role $role
     * @return bool
     */
    public function view(User $user, Role $role)
    {
        return $user->hasRole('super-admin') ||  $user->hasPermissionTo('view-role');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasRole('super-admin');
    }

    /**
     * @param User $user
     * @param Role $role
     * @return bool
     */
    public function update(User $user, Role $role)
    {
        return  $user->hasRole('super-admin') || $user->hasPermissionTo('update-role');
    }

    /**
     * @param User $user
     * @param Role $role
     * @return false
     */
    public function delete(User $user, Role $role)
    {
        return false;
    }

    /**
     * @param User $user
     * @param Role $role
     * @return false
     */
    public function restore(User $user, Role $role)
    {
        return false;
    }

    /**
     * @param User $user
     * @param Role $role
     * @return false
     */
    public function forceDelete(User $user, Role $role)
    {
        return false;
    }

    /**
     * Check whether the user can view the resource on Nova.
     *
     * @param User $user
     *
     * @return bool
     */
    public function viewAny(User $user)
    {
      return false;
//        return $user->hasRole('super-admin') || $user->hasPermissionTo('view-role');
    }

}
