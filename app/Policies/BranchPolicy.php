<?php

namespace App\Policies;

use App\Domains\Branch\Models\Branch;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BranchPolicy
{
    use HandlesAuthorization;

    /**
     * Check to blanket-add all permissions.
     *
     * @param User|null $user
     * @param $ability
     *
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->hasRole('super-admin')) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the branch.
     *
     * @param \App\User                       $user
     * @param \App\Domains\Branch\Models\Branch $branch
     *
     * @return mixed
     */
    public function view(User $user, Branch $branch)
    {
        return $user->hasPermissionTo('view-branch');
    }

    /**
     * Determine whether the user can create branches.
     *
     * @param \App\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create-branch');
    }

    /**
     * Determine whether the user can update the branch.
     *
     * @param \App\User                       $user
     * @param \App\Domains\Branch\Models\Branch $branch
     *
     * @return mixed
     */
    public function update(User $user, Branch $branch)
    {
        return $user->hasPermissionTo('update-branch');
    }

    /**
     * Determine whether the user can delete the branch.
     *
     * @param \App\User                       $user
     * @param \App\Domains\Branch\Models\Branch $branch
     *
     * @return mixed
     */
    public function delete(User $user, Branch $branch)
    {
        return false;
       // return $user->hasPermissionTo('delete-branch');
    }

    /**
     * Determine whether the user can restore the branch.
     *
     * @param \App\User                       $user
     * @param \App\Domains\Branch\Models\Branch $branch
     *
     * @return mixed
     */
    public function restore(User $user, Branch $branch)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the branch.
     *
     * @param \App\User                       $user
     * @param \App\Domains\Branch\Models\Branch $branch
     *
     * @return mixed
     */
    public function forceDelete(User $user, Branch $branch)
    {
        return false;
    }

    /**
     * Check whether the user can view the resource on Nova.
     *
     * @param User $user
     *
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('view-branch');
    }

  /* public function view(User $user, Branch $branch)
    {
        return $user->hasPermissionTo('view-branch');
    }

*/
}
