<?php

namespace App\Policies;

use App\Domains\Product\Models\AdditionalItem;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdditionItemPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param AdditionalItem $additionalItem
     * @return false
     */
    public function create(User $user)
    {
        return $user->hasRole('super-admin') ||  $user->hasPermissionTo('create-addition-item');
    }

    /**
     * @param User $user
     * @param AdditionalItem $additionalItem
     * @return false
     */
    public function update(User $user,  AdditionalItem $additionalItem)
    {
        return $user->hasRole('super-admin') ||  $user->hasPermissionTo('update-addition-item');
    }

    /**
     * @param User $user
     * @param AdditionalItem $additionalItem
     * @return false
     */
    public function delete(User $user, AdditionalItem $additionalItem)
    {
        return false;
    }
    /**
     * @param User $user
     * @param Product $product
     * @return false
     */
    public function viewAny(User $user)
    {
//        return false;
          return $user->hasRole('super-admin') ||  $user->hasPermissionTo('view-product');
    }

    public function view(User $user)
    {
//        return false;
          return $user->hasRole('super-admin') ||  $user->hasPermissionTo('view-product');
    }
}
