<?php

namespace App\Policies;

use App\Domains\Shipment\Models\Shipment;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ShipmentPolicy
{
    use HandlesAuthorization;


    /**
     * Check to blanket-add all permissions.
     *
     * @param User|null $user
     * @param $ability
     *
     * @return bool
     */
    public function before(User $user, $ability)
    {
//        if ($user->hasRole('super-admin')) {
//            return true;
//        }
    }

    /**
     * @param User $user
     * @param Role $role
     * @return bool
     */
    public function view(User $user, Shipment $shipment)
    {
        return $user->hasRole('super-admin') ||  $user->hasPermissionTo('view-shipment');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * @param User $user
     * @param Shipment $shipment
     * @return false
     */
    public function update(User $user, Shipment $shipment)
    {
        return false ;
    }

    /**
     * @param User $user
     * @param Shipment $shipment
     * @return false
     */
    public function delete(User $user, Shipment $shipment)
    {
        return false;
    }

    /**
     * @param User $user
     * @param Shipment $shipment
     * @return false
     */
    public function restore(User $user, Shipment $shipment)
    {
        return false;
    }

    /**
     * @param User $user
     * @param Shipment $shipment
     * @return false
     */
    public function forceDelete(User $user, Shipment $shipment)
    {
        return false;
    }

    /**
     * Check whether the user can view the resource on Nova.
     *
     * @param User $user
     *
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasRole('super-admin') || $user->hasRole('driver-management');
    }
}
