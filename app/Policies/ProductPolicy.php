<?php

namespace App\Policies;

use App\Domains\Product\Models\Product;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param Product $product
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasRole('super-admin') ||  $user->hasPermissionTo('create-product');
    }

    /**
     * @param User $user
     * @param Product $product
     * @return bool
     */
    public function update(User $user,  Product $product)
    {
        return $user->hasRole('super-admin') ||  $user->hasPermissionTo('update-product');
    }

    /**
     * @param User $user
     * @param Product $product
     * @return false
     */
    public function delete(User $user, Product $product)
    {
        return false;
    }

     /**
     * @param User $user
     * @param Product $product
     * @return false
     */
    public function viewAny(User $user)
    {
//        return false;
     return $user->hasRole('super-admin') ||  $user->hasPermissionTo('view-product');
    }
     public function view(User $user)
    {
//        return false;
     return $user->hasRole('super-admin') ||  $user->hasPermissionTo('view-product');
    }
}
