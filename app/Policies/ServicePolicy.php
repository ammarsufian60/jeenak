<?php

namespace App\Policies;

use App\Domains\Service\Models\Service;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ServicePolicy
{
    use HandlesAuthorization;
    /**
     * Check to blanket-add all permissions.
     *
     * @param User|null $user
     * @param $ability
     *
     * @return bool
     */
    public function before(User $user, $ability)
    {
//        if ($user->hasRole('super-admin')) {
//            return true;
//        }
    }

    /**
     * @param User $user
     * @param Service $service
     * @return bool
     */
    public function view(User $user, Service $service)
    {
        return $user->hasRole('super-admin') ||  $user->hasPermissionTo('view-service');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasRole('super-admin');
    }

    /**
     * @param User $user
     * @param Service $service
     * @return bool
     */
    public function update(User $user, Service $service)
    {
        return  $user->hasRole('super-admin') || $user->hasPermissionTo('update-sercie');
    }

    /**
     * @param User $user
     * @param Service $service
     * @return false
     */
    public function delete(User $user, Service $service)
    {
        return false;
    }

    /**
     * @param User $user
     * @param Service $service
     * @return false
     */
    public function restore(User $user, Service $service)
    {
        return false;
    }

    /**
     * @param User $user
     * @param Service $service
     * @return false
     */
    public function forceDelete(User $user, Service $service)
    {
        return false;
    }

    /**
     * Check whether the user can view the resource on Nova.
     *
     * @param User $user
     *
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasRole('super-admin') || $user->hasPermissionTo('view-service');
    }

}
