<?php

namespace App\Policies;

use App\Domains\Order\Models\Order;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Check to blanket-add all permissions.
     *
     * @param User|null $user
     * @param $ability
     *
     * @return bool
     */
    public function before(User $user, $ability)
    {
//        if ($user->hasRole('super-admin')) {
//            return true;
//        }
    }

    /**
     * Determine whether the user can view the order.
     *
     * @paraRolesTm \App\User                       $user
     * @param \App\Domains\Order\Models\Order $order
     *
     * @return mixed
     */
    public function view(User $user, Order $order)
    {
        return $user->hasRole('super-admin') ||  $user->hasPermissionTo('view-order');
    }

    /**
     * Determine whether the user can create orders.
     *
     * @param \App\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
//        return $user->hasPermissionTo('create-order');
    }

    /**
     * Determine whether the user can update the order.
     *
     * @param \App\User                       $user
     * @param \App\Domains\Order\Models\Order $order
     *
     * @return mixed
     */
    public function update(User $user, Order $order)
    {
        return false;
//        return $user->hasPermissionTo('update-order');
    }

    /**
     * Determine whether the user can delete the order.
     *
     * @param \App\User                       $user
     * @param \App\Domains\Order\Models\Order $order
     *
     * @return mixed
     */
    public function delete(User $user, Order $order)
    {
        return false;
        //return $user->hasPermissionTo('delete-order');
    }

    /**
     * Determine whether the user can restore the order.
     *
     * @param \App\User                       $user
     * @param \App\Domains\Order\Models\Order $order
     *
     * @return mixed
     */
    public function restore(User $user, Order $order)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the order.
     *
     * @param \App\User                       $user
     * @param \App\Domains\Order\Models\Order $order
     *
     * @return mixed
     */
    public function forceDelete(User $user, Order $order)
    {
        return false;
    }

    /**
     * Check whether the user can view the resource on Nova.
     *
     * @param User $user
     *
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasRole('super-admin') || $user->hasPermissionTo('view-order');
    }
}
