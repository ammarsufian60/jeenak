<?php

namespace App\Policies;

use App\Domains\Cart\Models\Cart;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CartPolicy
{
    use HandlesAuthorization;

    /**
     * Check to blanket-add all permissions.
     *
     * @param User|null $user
     * @param $ability
     *
     * @return bool
     */
    public function before(User $user, $ability)
    {
//        if ($user->hasRole('super-admin')) {
//            return true;
//        }
    }

    /**
     * Determine whether the user can view the cart.
     *
     * @param \App\User                     $user
     * @param \App\Domains\Cart\Models\Cart $cart
     *
     * @return mixed
     */
    public function view(User $user, Cart $cart)
    {
        return $user->hasRole('super-admin') || $user->hasPermissionTo('view-cart');
    }

    /**
     * Determine whether the user can create carts.
     *
     * @param \App\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
        //return $user->hasPermissionTo('create-cart');
    }

    /**
     * Determine whether the user can update the cart.
     *
     * @param \App\User                     $user
     * @param \App\Domains\Cart\Models\Cart $cart
     *
     * @return mixed
     */
    public function update(User $user, Cart $cart)
    {
        return false;
        //return $user->hasPermissionTo('update-cart');
    }

    /**
     * Determine whether the user can delete the cart.
     *
     * @param \App\User                     $user
     * @param \App\Domains\Cart\Models\Cart $cart
     *
     * @return mixed
     */
    public function delete(User $user, Cart $cart)
    {
        return false;
        //return $user->hasPermissionTo('delete-cart');
    }

    /**
     * Determine whether the user can restore the cart.
     *
     * @param \App\User                     $user
     * @param \App\Domains\Cart\Models\Cart $cart
     *
     * @return mixed
     */
    public function restore(User $user, Cart $cart)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the cart.
     *
     * @param \App\User                     $user
     * @param \App\Domains\Cart\Models\Cart $cart
     *
     * @return mixed
     */
    public function forceDelete(User $user, Cart $cart)
    {
        return false;
    }

    /**
     * Check whether the user can view the resource on Nova.
     *
     * @param User $user
     *
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasRole('super-admin') || $user->hasPermissionTo('view-cart');
    }

}
