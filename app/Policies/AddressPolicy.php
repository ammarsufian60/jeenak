<?php

namespace App\Policies;

use App\Domains\Address\Models\Address;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AddressPolicy
{
    use HandlesAuthorization;
    /**
     * @param User $user
     * @param Address $address
     * @return false
     */
    public function create(User $user)
    {
        return  false;
    }
    /**
     * @param User $user
     * @param Address $address
     * @return false
     */
    public function update(User $user, Address $address)
    {
        return  false;
    }

    /**
     * @param User $user
     * @param Address $address
     * @return false
     */
    public function delete(User $user, Address $address)
    {
        return false;
    }

    public function view(User $user)
    {
        return  $user->hasRole('super-admin');
    }
     
    public function viewAny(User $user)
    {
        return  $user->hasRole('super-admin');
    }	
}
