<?php

namespace App\Policies;

use App\Domains\Product\Models\ProductVariant;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductVariantPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param ProductVariant $productVariant
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasRole('super-admin') ||  $user->hasPermissionTo('create-product-variant');
    }

    /**
     * @param User $user
     * @param ProductVariant $productVariant
     * @return bool
     */
    public function update(User $user,  ProductVariant $productVariant)
    {
        return $user->hasRole('super-admin') ||  $user->hasPermissionTo('update-product-variant');
    }

    /**
     * @param User $user
     * @param ProductVariant $productVariant
     * @return false
     */
    public function delete(User $user,ProductVariant $productVariant)
    {
        return false;
    }
     /**
     * @param User $user
     * @param Product $product
     * @return false
     */
    public function viewAny(User $user)
    {
     //    return false;
     return $user->hasRole('super-admin') ||  $user->hasPermissionTo('view-product');
    }
     public function view(User $user)
    {
//        return false;
     return $user->hasRole('super-admin') ||  $user->hasPermissionTo('view-product');
    }
}
