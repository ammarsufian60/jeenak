<?php

namespace App\Policies;

use App\Domains\Account\Models\Account;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AccountPolicy
{
    use HandlesAuthorization;
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return false
     */
    public function create(User $user)
    {
        return  false;
    }

    /**
     * @param User $user
     * @param Account $account
     * @return false
     */
    public function update(User $user, Account $account )
    {
        return  false;
    }

    /**
     * @param User $user
     * @param Account $account
     * @return false
     */
    public function delete(User $user, Account $account)
    {
        return false;
    }

    public function view(User $user)
    {
        return  $user->hasRole('super-admin');
    }

    public function viewAny(User $user)
    {
        return  $user->hasRole('super-admin');
    }
}
