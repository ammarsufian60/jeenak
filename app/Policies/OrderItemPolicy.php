<?php

namespace App\Policies;

use App\Domains\Order\Models\OrderItem;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderItemPolicy
{
    use HandlesAuthorization;

    /**
     * Check to blanket-add all permissions.
     *
     * @param User|null $user
     * @param $ability
     *
     * @return bool
     */
    public function before(User $user, $ability)
    {
//        if ($user->hasRole('super-admin')) {
//            return true;
//        }
    }

    /**
     * Determine whether the user can view the order item.
     *
     * @param \App\User                               $user
     * @param \App\App\Domains\Order\Models\OrderItem $orderItem
     *
     * @return mixed
     */
    public function view(User $user, OrderItem $orderItem)
    {
        return $user->hasRole('super-admin') || $user->hasPermissionTo('view-order-item');
    }

    /**
     * Determine whether the user can create order items.
     *
     * @param \App\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
//        return $user->hasPermissionTo('create-order-item');
    }

    /**
     * Determine whether the user can update the order item.
     *
     * @param \App\User                               $user
     * @param \App\App\Domains\Order\Models\OrderItem $orderItem
     *
     * @return mixed
     */
    public function update(User $user, OrderItem $orderItem)
    {
        return false;
//        return $user->hasPermissionTo('update-order-item');
    }

    /**
     * Determine whether the user can delete the order item.
     *
     * @param \App\User                               $user
     * @param \App\App\Domains\Order\Models\OrderItem $orderItem
     *
     * @return mixed
     */
    public function delete(User $user, OrderItem $orderItem)
    {
        return false;
        //return $user->hasPermissionTo('delete-order-item');
    }

    /**
     * Determine whether the user can restore the order item.
     *
     * @param \App\User                               $user
     * @param \App\App\Domains\Order\Models\OrderItem $orderItem
     *
     * @return mixed
     */
    public function restore(User $user, OrderItem $orderItem)
    {
        return false;
        //return $user->hasPermissionTo('restore-order-item');
    }

    /**
     * Determine whether the user can permanently delete the order item.
     *
     * @param \App\User                               $user
     * @param \App\App\Domains\Order\Models\OrderItem $orderItem
     *
     * @return mixed
     */
    public function forceDelete(User $user, OrderItem $orderItem)
    {
        return false;
        //return $user->hasPermissionTo('force-delete-order-item');
    }

    /**
     * Check whether the user can view the resource on Nova.
     *
     * @param User $user
     *
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasRole('super-admin') || $user->hasPermissionTo('view-order-item');
    }

}
