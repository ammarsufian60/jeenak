<?php


namespace App\Facades;


use App\Domains\Package\PackageService;
use App\Domains\Rating\RatingService;
use Illuminate\Support\Facades\Facade;

class Package extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return PackageService::class;
    }

}
