<?php


namespace App\Facades;


use App\Domains\Category\CategoryService;
use Illuminate\Support\Facades\Facade;

class Category extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return CategoryService::class;
    }

}
