<?php


namespace App\Facades;


use App\Domains\Product\ProductSerivces;
use Illuminate\Support\Facades\Facade;

class Product extends Facade
{
    protected static function getFacadeAccessor()
    {
         return ProductSerivces::class;
    }

}
