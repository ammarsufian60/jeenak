<?php


namespace App\Facades;


use App\Domains\Brand\BrandService;
use Illuminate\Support\Facades\Facade;

class Brand extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return BrandService::class;
    }

}
