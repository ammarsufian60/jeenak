<?php


namespace App\Facades;


use App\Domains\Transaction\TransactionService;
use Illuminate\Support\Facades\Facade;

class Transaction extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return TransactionService::class;
    }
}
