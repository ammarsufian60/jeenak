<?php


namespace App\Facades;


use App\Domains\Cart\CartServices;
use Illuminate\Support\Facades\Facade;

class Cart extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
      return CartServices::class;
    }

}
