<?php

namespace App\Imports;

use App\Domains\User\Actions\SendOTP;
use App\Domains\User\Actions\SendSMSMessage;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class UserImport implements ToCollection
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        $message = Request()->message;
        collect($collection)->each(function($user)use($message){
            (new SendSMSMessage($user[3],$message))->handle();
        });

        return 'success';
    }
}
