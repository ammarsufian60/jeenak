<?php


use GuzzleHttp\Client;

if(! function_exists('haversign')) {
    function haversign($srtLat, $srcLong, $desLat, $destLong)
    {
        $earth_radius = 6371;

        $dLat = deg2rad($desLat - $srtLat);
        $dLon = deg2rad($destLong - $srcLong);

        $a = sin($dLat / 2) * sin($dLat / 2) + cos(deg2rad($srtLat)) * cos(deg2rad($desLat)) * sin($dLon / 2) * sin($dLon / 2);
        $c = 2 * asin(sqrt($a));
        $d = $earth_radius * $c;

        return floatval($d);
    }
}
if(! function_exists('distanceByGoogleDirections')) {
    function distanceByGoogleDirections($srtLat, $srcLong, $desLat, $destLong)
    {
       $client = new Client(['verified' => 'false']);

       $response = $client->get('https://maps.googleapis.com/maps/api/directions/json?origin='.$srtLat.','.$srcLong.'&destination='.$desLat.','.$destLong.'&key=AIzaSyD1jB0Hmtp8t8Pyy0Uf-pCvnBj7McS3dEU');

       $body = json_decode($response->getBody()->getContents());

       $distance = (float) $body->routes[0]->legs[0]->distance->text;

       return $distance;
    }
}
if(! function_exists('statusDictionary')) {
    function statusDictionary($status)
    {
        switch ($status){
            case 1:
                $status = 'available';
                break;
            case 0 :
                $status = 'busy';
                break;
            default:
                $status = 'not-avaliable';

        }
        return $status;
    }
}
