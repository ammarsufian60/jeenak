<?php

namespace App\Listeners;

use App\Domains\YallowDelivery\Requests\AddOrderRequest;
use App\Events\AddOrderToYellowSide;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class PushOrderIntoYellow implements ShouldQueue
{
   /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(AddOrderToYellowSide $event)
    {
        (new AddOrderRequest($event->order))->send();
    }
}
