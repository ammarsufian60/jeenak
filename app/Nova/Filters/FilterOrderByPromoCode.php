<?php

namespace App\Nova\Filters;

use App\Domains\Shipment\Models\PromoCode;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class FilterOrderByPromoCode extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    public $name ="Filter By Promo Code ";

    /**
     * @param Request $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $value
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where('promo_code_id', $value)->get();
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $promoCodes =  PromoCode::pluck('id','code');

        return $promoCodes;
    }
}
