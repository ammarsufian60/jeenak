<?php

namespace App\Domains\SuperWindow\Models;

use Spatie\MediaLibrary\File;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;


class SuperWindow extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $appends = ['super_window'];

    protected $fillable = ['is_arabic', 'redirect_url','is_mobile'];


    public function getSuperWindowAttribute()
    {
        $superWindow = $this->getFirstMedia('super_windows');

        if ( ! empty($superWindow)) {
            return $superWindow->getFullUrl();
        }
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('super_windows')
            ->acceptsFile(function (File $file) {
                return in_array($file->mimeType, [
                    'image/jpeg',
                    'image/png',
                ]);
            })
            ->singleFile();
    }
}
