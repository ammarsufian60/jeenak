<?php

namespace App\Domains\SuperWindow\Actions;

use App\Domains\SuperWindow\Models\SuperWindow;

class GetSuperWindowsList
{
    public function handle()
    {
        return SuperWindow::latest()->get();
    }
}
