<?php

namespace App\Domains\SuperWindow\Http\Controllers;

use App\Domains\SuperWindow\Actions\GetSuperWindowsList;
use App\Domains\SuperWindow\Http\Resources\SuperWindowResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SuperWindowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return SuperWindowResource::collection(
            (new GetSuperWindowsList)->handle()
        );
    }

}
