<?php

namespace App\Domains\SuperWindow\Http\Resources;

use App\Domains\ZainMiddleware\Http\Resources\PackageResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SuperWindowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'image' => $this->super_window,
            'redirect_url' => $this->redirect_url,
            'is_arabic' => $this->is_arabic,
        ];
    }
}
