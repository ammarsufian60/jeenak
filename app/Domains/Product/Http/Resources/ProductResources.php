<?php


namespace App\Domains\Product\Http\Resources;


use App\Domains\Brand\Http\Resources\BrandResource;
use App\Domains\Category\Http\Resources\CategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResources extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id ,
            'name' => $this->name ,
            'description' => $this->description ,
            'price' => $this->price ,
            'quantity' => $this->quantity ,
            'image' => is_null($this->image) ? "" : $this->image ,
            'brand' => new BrandResource($this->brand) ,
            'category' => new CategoryResource($this->category) ,

        ];

    }

}
