<?php


namespace App\Domains\Product\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class ProductVariantsResources extends JsonResource
{

    public function toArray($request)
    {
     return [
        'id' =>$this->id,
         'name'=> $this->name,
         'type' => $this->type,
         'price' => $this->pivot->price,
     ];

    }
}
