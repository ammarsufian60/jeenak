<?php


namespace App\Domains\Product\Http\Controllers;


use App\Domains\Product\Http\Resources\ProductResources;
use App\Domains\Product\ProductSerivces;
use App\Facades\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductsController extends Controller
{

    public function index(Request $request)
    {
        $products = Product::index($request->category_id);

        return ProductResources::collection($products);
    }
}
