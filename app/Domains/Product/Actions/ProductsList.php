<?php


namespace App\Domains\Product\Actions;


use App\Domains\Product\Models\Product;

class ProductsList
{
    protected $category_id;
    protected $brand_id;

    public function __construct($category_id=null)
    {
        $this->category_id = $category_id;
    }

    public function handle()
    {
        $products= Product::where('is_available',true);

           if(!is_null($this->category_id)){
               $products->where('category_id',$this->category_id);
           }
        return  $products->get();
    }
}
