<?php

namespace App\Domains\Product\Models;

use App\Domains\Brand\Models\Brand;
use App\Domains\Category\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\File;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;


class Product extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable= [
        'name',
        'description',
        'price',
        'quantity',
        'is_available',
        'category_id',
        'brand_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function variants()
    {
      return $this->hasMany(ProductVariant::class);
    }


    public function registerMediaCollections()
    {
        $this->addMediaCollection('product')
            ->acceptsFile(function (File $file) {
                return in_array($file->mimeType, [
                    'image/jpeg',
                    'image/png',
                ]);
            });
    }

    /**
     * @return string|null
     */
    public function getImageAttribute()
    {
        $image = $this->getFirstMediaUrl('product');
        return empty($image)? null :config('app.url') . $image;
    }

}
