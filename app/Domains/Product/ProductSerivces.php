<?php


namespace App\Domains\Product;


use App\Domains\Product\Actions\GetProductById;
use App\Domains\Product\Actions\ProductsList;

class ProductSerivces
{
    /**
     * @param $category_id
     * @return mixed
     */
    public function index($category_id=null)
    {
        return (new ProductsList($category_id))->handle();
    }

    /**
     * @param $product_id
     * @return mixed
     */
    public function getProductById($product_id)
    {
        return (new GetProductById($product_id))->handle();
    }

}
