<?php

namespace App\Domains\Brand\Http\Controllers;

use App\Facades\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domains\Brand\Http\Resources\BrandResource;

class BrandController extends Controller
{

    public function index(Request $request)
    {
    	$brands = Brand::paginate();

    	return  BrandResource::collection($brands);
    }


    public function show(Request $request, $brand)
    {
    	return new BrandResource(Brand::find($brand));
    }
}
