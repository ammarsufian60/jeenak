<?php

namespace App\Domains\Brand\Actions;

use App\Domains\Brand\Models\Brand;

class FindBrand
{
    public function handle(int $id)
    {
        return Brand::findOrFail($id);
    }
}
