<?php

namespace App\Domains\Brand\Actions;

use App\Domains\Brand\Models\Brand;

class GetBrandList
{
    /**
     * @return mixed
     */
    public function handle()
	{
	    $brands = Brand::get();
            return $brands;
	}
}
