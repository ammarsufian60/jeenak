<?php

namespace App\Domains\Brand\Models;

use Spatie\MediaLibrary\File;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Brand extends Model implements HasMedia
{
//    use SoftDeletes;
    use HasMediaTrait;

    /**
     * @var array
     */
    protected $appends = ['logo'];

    protected $with = ['media'];

    /**
     * @return string
     */
    public function getLogoAttribute()
    {
        $logo = $this->getFirstMedia('logos');

        if (!empty($logo)) {
            return $logo->getFullUrl();
        }
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('logos')
            ->acceptsFile(function (File $file) {
                return in_array($file->mimeType, [
                    'image/jpeg',
                    'image/png',
                ]);
            })
            ->singleFile();
    }

}
