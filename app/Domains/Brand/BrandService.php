<?php

namespace App\Domains\Brand;

use App\Domains\Brand\Actions\FindBrand;
use App\Domains\Brand\Actions\GetBrandList;

class BrandService
{
    /**
     * @return mixed
     */
    public function paginate()
    {
        return (new GetBrandList())->handle();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function find(int $id)
    {
        return (new FindBrand())->handle($id);
    }
}
