<?php


namespace App\Domains\Category\Http\Controller;


use App\Domains\Category\Http\Resources\CategoryResource;
use App\Facades\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index(Request $request)
    {
        try{
            $response = Category::index();
        }catch(\Exception $e){
            return response()->json(['message'=> $e->getMessage()],500);
        }
        return CategoryResource::collection($response);
    }
}
