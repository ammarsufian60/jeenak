<?php

namespace App\Domains\Category\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'=>$this->name,
            'name_arabic'=> $this->name_ar,
            "sub_categories" =>self::collection($this->subCategories)
        ];
    }
}
