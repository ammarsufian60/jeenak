<?php


namespace App\Domains\Category\Actions;


use App\Domains\Category\Models\Category;

class FindCategoryById
{
    /**
     * FindCategoryById constructor.
     * @param $category_id
     */
    public function __construct($category_id){
        $this->category_id = $category_id;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        return Category::find($this->category_id);
    }
}
