<?php


namespace App\Domains\Category\Actions;


use App\Domains\Category\Models\Category;

class IndexCategories
{
    /**
     * IndexCategories constructor.
     */
    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function handle()
    {
        return Category::whereNull('parent_category_id')->get();
    }
}
