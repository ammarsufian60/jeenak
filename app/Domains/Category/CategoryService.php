<?php


namespace App\Domains\Category;


use App\Domains\Category\Actions\IndexCategories;

class CategoryService
{
    /**
     * @return mixed
     */
    function index()
    {
        return (new IndexCategories())->handle();
    }
}
