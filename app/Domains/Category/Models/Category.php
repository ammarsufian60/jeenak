<?php

namespace App\Domains\Category\Models;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class Category extends Model
{
    protected $fillable =[
        'name',
        'name_ar',
        'parent_category_id',
        'state',

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Self::class,'parent_category_id')->whereNull('parent_category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subCategories()
    {
        return $this->hasMany(Self::class,'parent_category_id');
    }
}
