<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/**
 * Category api
 */
Route::get('categories','Category\Http\Controller\CategoryController@index');

/**
 * Brand apis
 */
Route::get('brands','Brand\Http\Controllers\BrandController@index');
Route::get('brands/{id}','Brand\Http\Controllers\BrandController@show');
/**
 * Super Window apis
 */
Route::get('super_window','SuperWindow\Http\Controllers\SuperWindowController@index');
/**
 * Product apis
 */
Route::get('/products','Product\Http\Controllers\ProductsController@index');


