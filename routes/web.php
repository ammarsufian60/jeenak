<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/**Route::get('/privacy-policy', function () {
    $privacy = \App\Domains\Service\Models\Service::find(4)->value;
    return view('Privacy',compact('privacy'));
});



Route::get('/invoices/{filters}','Order\Http\Controllers\PrintOrderController@PrintSelectOrdersInvoices');

Route::get('/invoice/{order_id}','Order\Http\Controllers\PrintOrderController@PrintOrderInvoice');
Route::get('/maps/{order_id}','Branch\Http\Controllers\MapsController');


Route::post('/yallow/order_created','YallowDelivery\Controllers\WebHookController@order_created');
*/
