<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
    <div class="content">
             <div class="title m-b-md">
                   Cova
             </div>
<br>
     <center>
    <div class="flex-center position-ref m-b-md" style="max-width:50%">
        This is a dynamic simulation for sms confirmation (otp) customized for international phone numbers outside SaudiArabia
    </div>
    </center>
        <div class="flex-center position-ref ">
           <table>
               <tr>
                   <th>Mobile Number</th>
                   <th>Otp</th>
               </tr>
               <tr>
                   <td>966596653315</td>
                   <td>8898</td>
               </tr>
           </table>
<br>

            </div>
        </div>
    </body>
</html>
