<html>

<head>
    <title> Cova Privacy And Policy</title>
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <style type="text/css">
        body {
            padding: 0px;
            margin: 0px;
            font-size: medium;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }

        .bold {
            font-weight: bold;
        }

        .navbar {
            padding: 20px;
        }

        .brandname {
            color: rgb(48, 47, 47);
           
        }

        .container {
            width: 90%;
            margin: auto;
        }

        .footer {
            width:100%;
            background-color:#f8f9fa;
            padding:20px;
        }
    </style>
</head>

<body>
<div class="navbar">
    <span class="brandname bold"> COVA</span>
    <span style="float: right;" class="brandname bold"> Privacy and Policy</span>
</div>
<div style="background-color: #f4623a;">
    <div class="navbar">
        <h3 style="text-align: center;color:white">  الشروط و الاحكام</h3>
        <div style="color:white;font-size:15pt;" class="container">{{$privacy}} </div>
    </div>
</div>
<div class="footer">
    <div class="navbar">
        <div style="text-align:center" >
            made with <i class="fas fa-x fa-heart text-primary" style="color:#f4623a"></i> by Cova Team.
        </div>
    </div>
</div>
</body>

</html>
