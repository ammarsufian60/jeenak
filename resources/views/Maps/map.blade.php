<html>
<head>
    <title>Nova Maps</title>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD2ZV89LrbTdgZsU4RKKxgTV0QUicxyacU&callback=initMap&libraries=&v=weekly"
        defer
    ></script>
    <style type="text/css">
        #map {
            height: 100%;
        }

        html,
        body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<body>
<div id="map"></div>
<label id="longitude" style="display: none" aria-label="{{$longitude}}">{{$longitude}}</label>
<label id="latitude" style="display: none" aria-label="{{$latitude}}">{{$latitude}}</label>
<script>
    let map;
    let latitude =  parseFloat(document.getElementById("latitude").textContent);
    let longitude = parseFloat(document.getElementById("longitude").textContent);

    function initMap() {

        map = new google.maps.Map(document.getElementById("map"), {
            center: { lat: latitude, lng: longitude },
            zoom: 15,
        });
        const marker = new google.maps.Marker({
            // The below line is equivalent to writing:
            // position: new google.maps.LatLng(-34.397, 150.644)
            position: { lat: latitude, lng: longitude },
            map: map,
        });
        // You can use a LatLng literal in place of a google.maps.LatLng object when
        // creating the Marker object. Once the Marker object is instantiated, its
        // position will be available as a google.maps.LatLng object. In this case,
        // we retrieve the marker's position using the
        // google.maps.LatLng.getPosition() method.
        const infowindow = new google.maps.InfoWindow({
            content: "<p> Order Customer Location:" + marker.getPosition() + "</p>",
        });
        google.maps.event.addListener(marker, "click", () => {
            infowindow.open(map, marker);
        });
    }

</script>
</body>
</html>
